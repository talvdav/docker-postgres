#!/bin/bash

VOLUME="./docker-data"
VOLUME_PGADMIN="$VOLUME/pgadmin-data"
VOLUME_POSTGRES="$VOLUME/postgres-data"

if [ -d $VOLUME_PGADMIN ]; then
    echo -e "\n\tINITIAL SETUP DONE"
    echo -e "\tSTARTING CONTAINERS"
    echo ""
    echo "docker-compose start"
    echo ""
    docker-compose start || echo -e "\n\trunning: docker-compose up -d\n\n"; docker-compose up -d
    
    echo -ne "\n\tPostgres IPAddress: "; docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' postgres
    echo ""
fi 
if [ ! -d $VOLUME ]; then
    if [ ! -d $VOLUME_PGADMIN ]; then 
        echo "creating $VOLUME_PGADMIN"
        mkdir -p $VOLUME_PGADMIN
    fi

    chown -R 5050:5050 $VOLUME_PGADMIN &&

    if [ ! -d $VOLUME_POSTGRES ]; then 
        echo "creating $VOLUME_POSTGRES"
        mkdir -p $VOLUME_POSTGRES 
    fi

    echo ""
    docker-compose up -d &&
    echo -ne "\n\tPostgres IPAddress: "; docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' postgres
    echo ""
fi